# -*- mode: python; mode: font-lock; py-indent-offset: 4; -*-
# $Id: setup.py 32 2008-03-11 12:58:07Z mok $

from distutils.core import setup

setup(name="pdbget",
      version="1.0",
      description="Utility to download entries from Protein Data Bank",
      author="Morten Kjeldgaard",
      author_email="mok@bioxray.dk",
      license="GPL",
      scripts=['pdbget'],
      requires=['pycurl']
      )

